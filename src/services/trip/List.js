import { Service } from "node-service-layer";
import { Op } from "sequelize";
import sequelize from "../../sequelize";
import { dumpTrip } from "../../utils/services/dumps";

const Trip = sequelize.model("Trip");

export default class List extends Service {
    static validate = {
        from : [ "string" ],
        to   : [ "string" ]
    };

    async execute({ from = "", to = "" }) {
        const trips = await Trip.findAll({
            where : {
                fromPoint : { [Op.like]: `%${from}%` },
                toPoint   : { [Op.like]: `%${to}%` }
            }
        });

        return trips.map(dumpTrip);
    }
}
