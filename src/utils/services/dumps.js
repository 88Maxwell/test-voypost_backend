export const dumpTrip = obj => ({
    id          : obj.id,
    fromPoint   : obj.fromPoint,
    toPoint     : obj.toPoint,
    departDate  : obj.departDate,
    vehicle     : obj.vehicle,
    description : obj.description
});

