import models from "../../models";

export default (sequelize, Sequelize) => {
    for (const modelKey in models) {
        if (models.hasOwnProperty(modelKey)) {
            const model = models[modelKey](sequelize, Sequelize);

            if (model.hasOwnProperty("initRelation")) model.initRelation();
        }
    }
};
