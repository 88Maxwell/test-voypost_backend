import { Exception } from "node-service-layer";
import LIVR from "livr";

export default {
    name    : "validate",
    type    : "required",
    execute : (ctx, args) => {
        let argsRule;

        if (typeof args === "object") {
            argsRule = args;
        } else if (typeof args === "function") {
            argsRule = args();
        }

        const validator = new LIVR.Validator(argsRule).prepare();
        const validData = validator.validate(ctx);

        if (validData) {
            return { ...validData, context: ctx.context };
        }

        throw new Exception({
            code   : "FORMAT_ERROR",
            fields : validator.getErrors()
        });
    }
};

