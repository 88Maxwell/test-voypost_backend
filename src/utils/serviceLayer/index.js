import argumentBuilder from "./argumentBuilder";
import validate from "./validate";

export function resolver(result) {
    this.body = result;
}

export const argumentsBuilder = args => args[0];

export const rules = {
    before : [ argumentBuilder, validate ],
    after  : [
        /* rule for your logger */
    ]
};
