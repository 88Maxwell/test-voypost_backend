import camelcase from "camelcase";

export default {
    name    : "argumentBuilder",
    type    : "hidden",
    execute : ctx => {
        const params = camelcaseObject(ctx.params);

        return { ...ctx.request.body, ...params, ...ctx.query };
    }
};

function camelcaseObject(obj) {
    const resultObject = {};
    const keys = Object.keys(obj);

    keys.forEach(key => {
        const value = typeof obj[key] === "object" && !Array.isArray(obj[key]) ? camelcaseObject(obj[key]) : obj[key];

        resultObject[camelcase(key)] = value;
    });

    return resultObject;
}
