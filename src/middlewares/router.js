import Router from "koa-router";
import ServiceLayer  from "../serviceLayer";
import TripList from "../services/trip/List";
import config from "../../configs/config.json";

const router = new Router({ prefix: config.PREFIX });

router.get("/trips", ServiceLayer.useService(TripList));

export default router;
