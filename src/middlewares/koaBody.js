import path from "path";
import koaBody from "koa-body";

export default koaBody({
    multipart  : true,
    formidable : { uploadDir: path.join(process.cwd(), "..", "temporary") }
});
