import Sequelize from "sequelize";
import dbConfig from "../configs/db.json";

import utils from "./utils/models";

const { database, username, password, dialect, host, port } = dbConfig[process.env.NODE_ENV];
const sequelize = new Sequelize(database, username, password, {
    insecureAuth : true,
    logging      : false,
    host,
    port,
    dialect
});

utils.init(sequelize, Sequelize);

export default sequelize;
