import { ServiceLayer } from "node-service-layer";
import { resolver, argumentsBuilder, rules } from "./utils/serviceLayer";

export default new ServiceLayer(resolver, argumentsBuilder, rules);
