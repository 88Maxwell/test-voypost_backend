import Koa from "koa";
import middlewares from "./middlewares";

const koaServer = new Koa();

koaServer.use(middlewares.helmet());
koaServer.use(middlewares.cors());
koaServer.use(middlewares.koaBody);
koaServer.use(middlewares.router.routes());

export default koaServer;
