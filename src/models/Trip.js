export default function (sequelize, Sequelize) {
    const Trip = sequelize.define("Trip", {
        id          : { type: Sequelize.UUID, defaultValue: Sequelize.UUIDV4, primaryKey: true },
        fromPoint   : { type: Sequelize.STRING, allowNull: false },
        toPoint     : { type: Sequelize.STRING, allowNull: false },
        departDate  : { type: Sequelize.DATE, allowNull: false },
        vehicle     : { type: Sequelize.STRING, allowNull: false },
        description : { type: Sequelize.STRING, allowNull: false },
        createdAt   : { type: Sequelize.DATE,  defaultValue: new Date() },
        updatedAt   : { type: Sequelize.DATE, defaultValue: new Date() }
    });

    return Trip;
}
