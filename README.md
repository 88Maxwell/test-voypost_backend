# Voypost_test[backend]

## Usage

MySql and Node is required !

### Development
1. yarn 
2. cp configs/config.json.sample configs/config.json 
3. cp configs/db.json.sample configs/db.json 
4. yarn db:create development
5. yarn db:migrate development
6. yarn db:seeders development
7. setup configs
8. yarn dev

### Test
1. yarn 
2. cp configs/config.json.sample configs/config.json 
3. cp configs/db.json.sample configs/db.json 
4. setup configs
5. yarn db:create test
6. yarn db:migrate test
7. yarn test

### Production
1. yarn 
2. cp configs/config.json.sample configs/config.json 
3. cp configs/db.json.sample configs/db.json 
4. setup configs
5. yarn db:create production
6. yarn db:migrate production
7. yarn db:seeders production
8. yarn start
