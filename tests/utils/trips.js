import { dumpTrip } from "../../src/utils/services/dumps";

import { create } from "../mocks/trips";
import sequelize from "../../src/sequelize";

const Trip = sequelize.model("Trip");

export async function generateTrip({
    fromPoint = "Bedychiv, Ukraine",
    toPoint = "Amsterdam, Netherland",
    description = "Nice"
} = {}) {
    const collection = await Trip.create({ ...create.data, description, fromPoint, toPoint });

    return dumpTrip(collection);
}

export const generateTrips = async (dirrections = []) =>
    Promise.all(dirrections.map((dirrection, index) => generateTrip({
        fromPoint   : dirrection.fromPoint || `from ${index}`,
        toPoint     : dirrection.toPoint || `to ${index}`,
        description : `Trip ${index}/${dirrections.length}`
    })));
