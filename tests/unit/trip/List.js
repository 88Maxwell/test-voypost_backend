import { expect } from "chai";
import { matchLIVR, request, cleanup } from "../..";
import { generateTrips } from "../../utils/trips";
import { fullListExpect } from "../../mocks/trips";

suite("Trip List");

const fullListSize = 6;

before(async () => {
    const arrayOfEmptyObj = Array.from(Array(fullListSize)).map(() => ({}));

    await generateTrips(arrayOfEmptyObj);
});


test("Positive : Get all trips", async () => {
    await request
        .get("/api/v1/trips")
        .expect(200)
        .expect(res => {
            matchLIVR(res.body, fullListExpect);
            expect(res.body.data.length).to.equal(fullListSize);
        });
});

test("Positive : Search request from=2", async () => {
    await request
        .get(`/api/v1/trips?from=${2}`)
        .expect(200)
        .expect(res => {
            matchLIVR(res.body, fullListExpect);
            expect(res.body.data.length).to.equal(1);
        });
});


test("Positive :  Search request from=2 to", async () => {
    await request
        .get(`/api/v1/trips?from=${2}&to=${1}`)
        .expect(200)
        .expect(res => {
            matchLIVR(res.body, fullListExpect);
            expect(res.body.data.length).to.equal(0);
        });
});

test("Positive : Get all trips", async () => {
    await request
        .get(`/api/v1/trips?from=${2}&to=${2}`)
        .expect(200)
        .expect(res => {
            matchLIVR(res.body, fullListExpect);
            expect(res.body.data.length).to.equal(1);
        });
});


after(async () => {
    await cleanup();
});
