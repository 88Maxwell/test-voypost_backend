export const create = {
    data : {
        fromPoint   : "Bedychiv, Ukraine",
        toPoint     : "Amsterdam, Netherland",
        departDate  : new Date(),
        vehicle     : "car",
        description : "better than yesterday"
    }
};

export const createExpect = {
    status : { is: 200 },
    data   : [ "required", { "nested_object" : {
        id          : [ "required" ],
        fromPoint   : [ "required", "string" ],
        toPoint     : [ "required", "string" ],
        departDate  : [ "required", "string" ],
        vehicle     : [ "required", "string" ],
        description : [ "required", "string" ]
    } } ]
};

export const fullListExpect = {
    status : { is: 200 },
    data   : [ "required", { "list_of_objects" : {
        id          : [ "required" ],
        fromPoint   : [ "required", "string" ],
        toPoint     : [ "required", "string" ],
        departDate  : [ "required", "string" ],
        vehicle     : [ "required", "string" ],
        description : [ "required", "string" ]
    } } ]
};
