// eslint-disable-next-line import/no-commonjs
module.exports = {
    up : (queryInterface, Sequelize) => {
        return queryInterface.createTable("Trips", {
            id          : { type: Sequelize.UUID, defaultValue: Sequelize.UUIDV4, primaryKey: true },
            fromPoint   : { type: Sequelize.STRING, allowNull: false },
            toPoint     : { type: Sequelize.STRING, allowNull: false },
            departDate  : { type: Sequelize.DATE, allowNull: false },
            vehicle     : { type: Sequelize.STRING, allowNull: false },
            description : { type: Sequelize.STRING, allowNull: false },
            createdAt   : Sequelize.DATE,
            updatedAt   : Sequelize.DATE
        });
    },

    down : queryInterface => {
        return queryInterface.dropTable("Trips");
    }
};
