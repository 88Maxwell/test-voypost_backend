// eslint-disable-next-line import/no-commonjs
module.exports = {
    up : async queryInterface => {
        await queryInterface.bulkInsert(
            "Trips",
            [
                // 1
                {
                    id          : "2ccfdda0-2c42-430d-b5ff-dd1466702f6d",
                    departDate  : new Date("2019-05-29T00:00:00.000Z"),
                    vehicle     : "plane",
                    description : "Nice",
                    fromPoint   : "Berlin, Germany",
                    toPoint     : "Kyiv, Ukraine"
                },
                // 2
                {
                    id          : "0b968392-7868-4101-97fc-a23a4bc891e2",
                    departDate  : new Date("2019-06-02T00:00:00.000Z"),
                    vehicle     : "car",
                    description : "Nice",
                    fromPoint   : "Berlin, Germany",
                    toPoint     : "Dnipro, Ukraine"
                },
                // 3
                {
                    id          : "071af9ca-801b-4ea7-a903-de241727fd0a",
                    departDate  : new Date("2019-06-07T00:00:00.000Z"),
                    vehicle     : "plane",
                    description : "Nice",
                    fromPoint   : "London, UK",
                    toPoint     : "Kyiv, Ukraine"
                },
                // 4
                {
                    id          : "a3b187d1-2770-4162-8b39-b4cf13c2580c",
                    departDate  : new Date("2019-06-07T00:00:00.000Z"),
                    vehicle     : "plane",
                    description : "Nice",
                    fromPoint   : "Lyon, France",
                    toPoint     : "Kyiv, Ukraine"
                },
                // 5
                {
                    id          : "35eb8ac7-4899-487a-a930-3f5e6fdc0dcc",
                    departDate  : new Date("2019-06-08T00:00:00.000Z"),
                    vehicle     : "plane",
                    description : "Nice",
                    fromPoint   : "Moscow, Russia",
                    toPoint     : "Kyiv, Ukraine"
                },
                // 6
                {
                    id          : "fcc93c5b-3103-43b6-b972-b3076866a860",
                    departDate  : new Date("2019-05-30T00:00:00.000Z"),
                    vehicle     : "train",
                    description : "Nice",
                    fromPoint   : "Berlin, Germany",
                    toPoint     : "Kyiv, Ukraine"
                }
            ],
            {}
        );
    },

    down : async queryInterface => {
        await queryInterface.bulkDelete("Trips", null, {});
    }
};
